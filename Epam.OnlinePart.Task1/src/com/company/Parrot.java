package com.company;

public class Parrot implements Animal, Bird {

    @Override
    public void sleep() {
        System.out.println("Parrots usually sleep 6-8 hours.");
    }

    @Override
    public void eat() {
        System.out.println("Parrots eat plants.");
    }

    @Override
    public void fly() {
        System.out.println("Parrots fly up to 2,000 feet.");
    }
}

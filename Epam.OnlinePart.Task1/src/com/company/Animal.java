package com.company;

public interface Animal {
    public void sleep();
    public void eat();
}

package com.company;

public class Eagle implements Animal, Bird{
    @Override
    public void sleep() {
        System.out.println("Eagles usually sleep 7-8 hours.");
    }

    @Override
    public void eat() {
        System.out.println("Eagles eat meat.");
    }

    @Override
    public void fly() {
        System.out.println("Eagles fly up to 10,000 feet.");
    }
}

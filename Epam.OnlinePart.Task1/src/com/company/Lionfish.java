package com.company;

public class Lionfish implements Animal, Fish{
    @Override
    public void swim() {
        System.out.println("Unusually large lionfish swim at depths of up to 300 fee.");
    }

    @Override
    public void sleep() {
        System.out.println("Lionfish sleep upside down.");
    }

    @Override
    public void eat() {
        System.out.println("Lionfish eat grouper and other large fish.");
    }
}

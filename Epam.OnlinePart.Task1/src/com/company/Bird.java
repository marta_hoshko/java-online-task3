package com.company;

public interface Bird  extends Animal{
    int numberOfLegs = 2;
    String covering = "feather";
    String  tail = "colorful long tail";

    public void fly();
}

package com.company;

public class Main {

    public static void main(String[] args) {
        Eagle myEagle = new Eagle();
        myEagle.eat();
        myEagle.fly();
        myEagle.sleep();
        System.out.println("Eagles have"+ ' ' + Bird.numberOfLegs+ ' '+"legs"+'.');
        System.out.println("Eagles are covered by" + ' ' + Bird.covering+'.');
        System.out.println("------------------------------------------------------------");
        Parrot myParrot = new Parrot();
        myParrot.eat();
        myParrot.fly();
        myParrot.sleep();
        System.out.println("Parrots have"+ ' ' + Bird.numberOfLegs+ ' '+"legs"+'.');
        System.out.println("Parrots are covered by" + ' ' + Bird.covering+' '+"and also have"+' '+ Bird.tail+'.');
        System.out.println("------------------------------------------------------------");
        Lionfish myLionfish = new Lionfish();
        myLionfish.eat();
        myLionfish.swim();
        myLionfish.sleep();
        System.out.println("Lionfish has"+' '+Fish.spine);
    }
}
